package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type SessionInput struct {
	RoomId   int    `json:"room_id"`
	Date     string `json:"date"`
	Session_ string `json:"session"`
	Status   string `json:"status"`
}

func SessionRead(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := `SELECT * FROM session`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sessionrsl []models.Session
	for rows.Next() {
		session := models.Session{}
		rows.Scan(&session.SessionId, &session.RoomId, &session.Date, &session.Session_, &session.Status)
		sessionrsl = append(sessionrsl, session)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Session Success", "data": sessionrsl})

}

func SessionCreate(c *gin.Context) {
	var input SessionInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Session{}
	p.RoomId = input.RoomId
	p.Date = input.Date
	p.Session_ = input.Session_
	p.Status = input.Status

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := fmt.Sprintf(`INSERT INTO session(room_id, date, session, status) VALUES ('%d','%s', '%s', '%s')`, p.RoomId, p.Date, p.Session_, p.Status)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Session Success"})

}

func SessionUpdate(c *gin.Context) {
	var input SessionInput
	var sessionID = c.Param("session_id")
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Session{}
	p.RoomId = input.RoomId
	p.Date = input.Date
	p.Session_ = input.Session_
	p.Status = input.Status

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//update session to database
	tsql := fmt.Sprintf(`UPDATE session SET room_id = '%d',  date = '%s', session ='%s', status='%s' WHERE session.session_id = '%s'`, p.RoomId, p.Date, p.Session_, p.Status, sessionID)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Session Success"})

}

func SessionDelete(c *gin.Context) {
	var sessionID = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//update session to database
	tsql := fmt.Sprintf(`DELETE FROM session WHERE session.session_id='%s'`, sessionID)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Session Success"})

}
