package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type AttendanceInput struct {
	ApplicantId int    `json:"applicant_id"`
	SessionId   int    `json:"session_id"`
	CreateDate  string `json:"created_date"`
}

func AttendanceRead(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//selected attendance to database
	tsql := `SELECT * FROM attendance`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var attendancersl []models.Attendance
	for rows.Next() {
		attendance := models.Attendance{}
		rows.Scan(&attendance.ApplicantId, &attendance.SessionId, &attendance.CreateDate)
		attendancersl = append(attendancersl, attendance)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Attendance Success", "data": attendancersl})

}

func AttendanceCreate(c *gin.Context) {
	var input AttendanceInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Attendance{}
	p.ApplicantId = input.ApplicantId
	p.SessionId = input.SessionId

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert attendance to database
	tsql := fmt.Sprintf(`INSERT INTO attendance(applicant_id, session_id, created_date) VALUES ('%d','%d', now())`, p.ApplicantId, p.SessionId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Attendance Success"})
}

func AttendanceUpdate(c *gin.Context) {
	var input AttendanceInput
	var applicantId = c.Param("applicant_id")
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Attendance{}
	p.ApplicantId = input.ApplicantId
	p.SessionId = input.SessionId

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//Update Attendance to database
	tsql := fmt.Sprintf(`UPDATE attendance SET applicant_id='%d', session_id = '%d', created_date =now() WHERE attendance.applicant_id = '%s'`, p.ApplicantId, p.SessionId, applicantId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Attendance Success"})
}

func AttendanceDelete(c *gin.Context) {
	var input AttendanceInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Attendance{}
	p.ApplicantId = input.ApplicantId
	p.SessionId = input.SessionId
	p.CreateDate = input.CreateDate
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//Update Attendance to database
	tsql := fmt.Sprintf(`DELETE FROM attendance WHERE applicant_id= '%d' AND session_id ='%d'`, p.ApplicantId, p.SessionId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Attendance Success"})
}
