package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"

	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type RoleInput struct {
	RoleId   int    `json:"role_id"`
	RoleName string `json:"role_name"`
}

func RoleCreat(c *gin.Context) {
	var input RoleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Role{}
	p.RoleName = input.RoleName

	//DB Connect
	DBURL := models.DbConnect()
	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := fmt.Sprintf(`INSERT INTO role (role_name) VALUES ('%s')`, p.RoleName)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Role Success"})

}

func RoleRead(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Read protoring to database
	tsql := `SELECT * FROM role`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var rolersl []models.Role
	for rows.Next() {
		role := models.Role{}
		rows.Scan(&role.RoleId, &role.RoleName)
		rolersl = append(rolersl, role)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Role Success", "data": rolersl})
}

func RoleUpdate(c *gin.Context) {
	var input RoleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Role{}
	p.RoleId = input.RoleId
	p.RoleName = input.RoleName

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//update protoring to database
	tsql := fmt.Sprintf(`UPDATE role SET role_id = '%d',  role_name  = '%s' WHERE role_id = '%d'`, p.RoleId, p.RoleName, p.RoleId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Role Success"})
}

func RoleDelete(c *gin.Context) {
	var roleId = c.Param("role_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//insert report to database
	tsql := fmt.Sprintf(`DELETE FROM role WHERE role_id= '%s'`, roleId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Role Success"})

}
