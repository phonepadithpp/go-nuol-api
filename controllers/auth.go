package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"html"
	"log"
	"net/http"
	"os"
	"strings"

	"go-nuol-api/utils/token"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

type RegisterInput struct {
	Name         string `json:"name" binding:"required"`
	FromFaculty  string `json:"from_faculty" binding:"required"`
	Username     string `json:"username" binding:"required"`
	Password     string `json:"password" binding:"required"`
	RoleId       int    `json:"role_id"`
	ModifiedDate string `json:"modified_date"`
}

type LoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func CurrentUser(c *gin.Context) {

	proctor_id, err := token.ExtractTokenID(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u, err := models.GetUserByID(proctor_id)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "success", "data": u})
}

//Login
func Login(c *gin.Context) {

	var input LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.Proctor{}

	u.Username = input.Username
	u.Password = input.Password

	token, err := models.LoginCheck(u.Username, u.Password)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	if token == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"token": token})

}

//Register
func Register(c *gin.Context) {

	var input RegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Proctor{}

	p.Name = input.Name
	p.FromFaculty = input.FromFaculty
	p.Username = input.Username
	p.Password = input.Password
	p.RoleId = input.RoleId

	//load .env fie to connect database
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	DbHost := os.Getenv("DB_HOST")
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")
	DbPort := os.Getenv("DB_PORT")

	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//encrpt password
	//turn password into hash
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(p.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err.Error())
	}

	p.Password = string(hashedPassword)

	//remove spaces in username
	p.Username = html.EscapeString(strings.TrimSpace(p.Username))

	//insert protor to database
	tsql := fmt.Sprintf(`INSERT INTO proctor(name,from_faculty,username,password,role_id,modified_date) VALUES ('%s','%s','%s','%s','%d', NOW())`, p.Name, p.FromFaculty, p.Username, p.Password, p.RoleId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "registration success"})

}
