package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type ReportInput struct {
	ReportId   int    `json:"report_id" `
	SessionId  int    `json:"session_id" `
	Detail     string `json:"detail"`
	CreateDate string `json:"created_date"`
}

func GetReportReadAll(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := `SELECT * FROM report`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var Data []models.Report
	for rows.Next() {
		report := models.Report{}
		rows.Scan(&report.ReportId, &report.SessionId, &report.Detail, &report.CreateDate)
		Data = append(Data, report)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Report Success", "data": Data})

}

func GetReportRead(c *gin.Context) {
	var ReportId = c.Param("report_id")
	if ReportId == "" {
		panic("Error! request")
	}

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`SELECT * FROM report WHERE report_id=%s`, ReportId)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var reportrsl []models.Report
	for rows.Next() {
		report := models.Report{}
		rows.Scan(&report.ReportId, &report.SessionId, &report.Detail, &report.CreateDate)
		reportrsl = append(reportrsl, report)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Report Success", "data": reportrsl})

}

func GetReportReadBySession(c *gin.Context) {
	var SessionId = c.Param("session_id")

	if SessionId == "" {
		panic("Error! request")
	}
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`SELECT * FROM report WHERE session_id=%s`, SessionId)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var Data []models.Report
	for rows.Next() {
		report := models.Report{}
		rows.Scan(&report.ReportId, &report.SessionId, &report.Detail, &report.CreateDate)
		Data = append(Data, report)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Report Success", "data": Data})

}

func ReportCreate(c *gin.Context) {
	var input ReportInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Report{}
	p.ReportId = input.ReportId
	p.SessionId = input.SessionId
	p.Detail = input.Detail

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert report to database
	tsql := fmt.Sprintf(`INSERT INTO report(session_id, detail, created_date) VALUES ('%d','%s', now())`, p.SessionId, p.Detail)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Report Success"})
}

func ReportUpdate(c *gin.Context) {
	var input ReportInput
	var reportId = c.Param("report_id")
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Report{}
	p.ReportId = input.ReportId
	p.SessionId = input.SessionId
	p.Detail = input.Detail

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//insert report to database
	tsql := fmt.Sprintf(`UPDATE report SET session_id = '%d',  detail = '%s', created_date =now() WHERE report.report_id = '%s'`, p.SessionId, p.Detail, reportId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Report Success"})
}

func ReportDelete(c *gin.Context) {

	var reportId = c.Param("report_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//insert report to database
	tsql := fmt.Sprintf(`DELETE FROM report WHERE report_id= '%s'`, reportId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Report Success"})
}
