package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type RuleBreakerInput struct {
	ApplicantId int    `json:"applicant_id"`
	SessionId   int    `json:"session_id"`
	Fault       string `json:"fault"`
	Penalty     string `json:"penalty"`
	CreateDate  string `json:"created_date"`
}

func RuleBreakerRead(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Get Rule Breaker
	tsql := `SELECT * FROM rule_breaker`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var rulbreakerrsl []models.RuleBreaker
	for rows.Next() {
		rulebreaker := models.RuleBreaker{}
		rows.Scan(&rulebreaker.ApplicantId, &rulebreaker.SessionId, &rulebreaker.Fault, &rulebreaker.Penalty, &rulebreaker.CreateDate)
		rulbreakerrsl = append(rulbreakerrsl, rulebreaker)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Rule Breaker Success", "data": rulbreakerrsl})

}

func RuleBreakerCreate(c *gin.Context) {
	var input RuleBreakerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.RuleBreaker{}

	p.ApplicantId = input.ApplicantId
	p.SessionId = input.SessionId
	p.Fault = input.Fault
	p.Penalty = input.Penalty

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert Rule Breaker to database
	tsql := fmt.Sprintf(`INSERT INTO rule_breaker(applicant_id, session_id, fault, penalty, created_date) VALUES ('%d','%d', '%s', '%s', now())`, p.ApplicantId, p.SessionId, p.Fault, p.Penalty)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Rule Breaker Success"})

}

func RuleBreakerUpdate(c *gin.Context) {
	var ApplicantParam = c.Param("applicant_id")
	var input RuleBreakerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.RuleBreaker{}

	p.SessionId = input.SessionId
	p.Fault = input.Fault
	p.Penalty = input.Penalty

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Update Rule Breaker to database
	tsql := fmt.Sprintf(`UPDATE rule_breaker SET session_id = '%d', fault ='%s', penalty='%s', created_date=now() WHERE rule_breaker.applicant_id = '%s'`, p.SessionId, p.Fault, p.Penalty, ApplicantParam)

	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Rule Breaker Success"})

}

func RuleBreakerDelete(c *gin.Context) {
	var ApplicantParam = c.Param("applicant_id")

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Delete Rule Breaker to database
	tsql := fmt.Sprintf(`DELETE FROM rule_breaker WHERE applicant_id = '%s'`, ApplicantParam)

	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Rule Breaker Success"})

}
