package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"html"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"

	//"github.com/gobuffalo/nulls"
	"go-nuol-api/utils/token"

	"golang.org/x/crypto/bcrypt"
)

func ProctorUpdate(c *gin.Context) {
	var input models.Proctor
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		// return
	}

	p := models.Proctor{}
	p.ProctorId = input.ProctorId
	p.Name = input.Name
	p.FromFaculty = input.FromFaculty
	p.Username = input.Username
	p.Password = input.Password
	p.RoleId = input.RoleId

	proctor_id, _ := token.ExtractTokenID(c)
	user, _ := models.GetUserByID(proctor_id)
	fmt.Println(user.RoleId)

	if p.ProctorId < 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error, Can't update"})
		return
	}

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := "UPDATE proctor SET"

	if user.RoleId == 1 {
		//Check if User is Admin
		if p.Name != "" {
			tsql = tsql + " name = '" + p.Name + "',"
		}

		if p.FromFaculty != "" {
			tsql = tsql + " from_faculty = '" + p.FromFaculty + "', "
		}

		if p.Username != "" {
			tsql = tsql + " username = '" + p.Username + "',"
		}
		if p.RoleId > 0 {
			tsql = tsql + fmt.Sprintf(` role_id = '%d',`, p.RoleId)
		}

	}

	if p.Password != "" {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(p.Password), bcrypt.DefaultCost)
		if err != nil {
			panic(err.Error())
		}
		p.Password = string(hashedPassword)
		tsql = tsql + " password = '" + p.Password + "',"
	}

	tsql = tsql + fmt.Sprintf(` modified_date = now() WHERE proctor.proctor_id = '%d'`, p.ProctorId)
	fmt.Println(tsql)
	insert, err := db.Query(tsql)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		panic(err.Error())
	} else {
		c.JSON(http.StatusOK, gin.H{"message": "Modified Success"})
	}
	defer insert.Close()

}

// Delete Proctor
func ProctorDelete(c *gin.Context) {
	var proctorId = c.Param("proctor_id")

	proctor_id, _ := token.ExtractTokenID(c)
	user, _ := models.GetUserByID(proctor_id)
	println(user.RoleId)

	if user.RoleId != 1 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Error, permission denied"})
		return
	}

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//insert report to database
	tsql := fmt.Sprintf(`DELETE FROM proctor WHERE proctor_id!=1 and proctor_id= '%s'`, proctorId)

	delete, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer delete.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Deleting Success"})
}

//Import Proctor from Excel
func ImportProctors(c *gin.Context) {

	proctor_id, _ := token.ExtractTokenID(c)
	user, _ := models.GetUserByID(proctor_id)
	if user.RoleId != 1 {
		panic("Permission denied")
	}

	type Data struct {
		Name     string `json:"proctor"`
		Date     string `json:"date"`
		RoomName string `json:"room_name"`
		Faculty  string `json:"faculty"`
		Username string `json:"username"`
		Password string `json:"password"`
	}
	type DataFeild struct {
		SiteName string `json:"site_name"`
		Data     []Data `json:"data"`
	}

	var input DataFeild

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var session_id int
	var room_id int
	var log []string

	for k, s := range input.Data {
		if s.Username == "" {
			continue
		}
		hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(s.Password), bcrypt.DefaultCost)
		Password := string(hashedPassword)
		Name := html.EscapeString(strings.TrimSpace(s.Name))
		RoomName := html.EscapeString(strings.TrimSpace(s.RoomName))
		Username := html.EscapeString(strings.TrimSpace(s.Username))
		Faculty := html.EscapeString(strings.TrimSpace(s.Faculty))
		Date := html.EscapeString(strings.TrimSpace(s.Date))
		RoleId := 2
		event := ""

		// println(fmt.Sprintf(`INSERT INTO foe.proctor VALUES (null, '%s', '%s', '%s', '%s', %d, NOW() )`, Name, Faculty, Username, Password, RoleId))
		insert, err := db.Exec(fmt.Sprintf(`INSERT INTO foe.proctor VALUES (null, '%s', '%s', '%s', '%s', %d, NOW() )`, Name, Faculty, Username, Password, RoleId))
		if err != nil {
			log = append(log, err.Error())
			fmt.Println(err)
			continue
		} else {
			event = fmt.Sprint(k) + ": Proctor " + Name + " inserted"
		}
		proctor_id, _ := insert.LastInsertId()

		sql_find_room := fmt.Sprintf(`SELECT er.id, ex.site_name FROM lums_data.sis_exam_room as er left join lums_data.sis_exam_building as eb on(er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on(eb.exam_site_id=ex.id) WHERE er.room_name='%s'`, RoomName)
		rows, _ := db.Query(sql_find_room)
		var RoomId, Id int
		var SiteName string
		RoomId = 0
		for rows.Next() {
			rows.Scan(&Id, &SiteName)
			if SiteName == input.SiteName {
				RoomId = Id
			}
		}

		if RoomId < 1 {
			event = event + ", The exam room " + RoomName + " is not existing"
			log = append(log, event)
			fmt.Println(err.Error())
			continue
		}

		sql_find_session := fmt.Sprintf(`SELECT er.id, se.session_id FROM lums_data.sis_exam_room as er left join foe.session as se on (se.room_id=er.id) WHERE er.id='%d' AND se.date='%s'`, RoomId, Date)
		rowNo, _ := db.Exec(sql_find_session)
		rowAf, _ := rowNo.RowsAffected()

		if rowAf < 1 {
			db.Exec(fmt.Sprintf(`INSERT INTO foe.session VALUES (null, '%d', '%s', 'ພາກເຊົ້າ', 'Open')`, RoomId, Date))
			db.Exec(fmt.Sprintf(`INSERT INTO foe.session VALUES (null, '%d', '%s', 'ພາກບ່າຍ', 'Open')`, RoomId, Date))
		}

		rows1, err := db.Query(sql_find_session)
		if err != nil {
			fmt.Println(err.Error())
		}
		defer rows1.Close()

		for rows1.Next() {
			rows1.Scan(&room_id, &session_id)
			if room_id > 0 && session_id > 0 {
				_, err := db.Exec(fmt.Sprintf(`INSERT INTO foe.proctoring VALUES (%d, %d)`, session_id, proctor_id))
				if err != nil {
					event = event + ", Some missing: The proctor " + Name + " is not assigned to " + RoomName + " session date " + Date
					fmt.Println(err.Error())
				}
			} else {
				event = event + ", The exam room " + RoomName + " on date " + Date + " is not existing"
				log = append(log, event)
			}
		}

		log = append(log, event)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "import success", "log": log})

}
