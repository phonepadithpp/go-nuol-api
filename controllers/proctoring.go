package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"

	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type ProctoringInput struct {
	SessionId int `json:"session_id"`
	ProctorId int `json:"proctor_id"`
}

func ProctoringCreat(c *gin.Context) {
	var input ProctoringInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Proctoring{}
	p.SessionId = input.SessionId
	p.ProctorId = input.ProctorId

	//DB Connect
	DBURL := models.DbConnect()
	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
    // Check conflict 
	// tsqlck := fmt.Sprintf(`select * from foe.proctoring as pg left join foe.session as se on (pg.session_id=se.session_id) where pg.proctor_id=%d`, p.ProctorId)

	//insert protoring to database
	tsql := fmt.Sprintf(`INSERT INTO proctoring(session_id,proctor_id) VALUES ('%d','%d')`, p.SessionId, p.ProctorId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Insert Proctoring Success"})

}

func ProctoringRead(c *gin.Context) {

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Read protoring to database
	tsql := `SELECT * FROM proctoring`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var proctoringrsl []models.Proctoring
	for rows.Next() {
		proctoring := models.Proctoring{}
		rows.Scan(&proctoring.SessionId, &proctoring.ProctorId)
		proctoringrsl = append(proctoringrsl, proctoring)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Proctoring Success", "data": proctoringrsl})
}

func ProctoringUpdate(c *gin.Context) {
	var input ProctoringInput
	var sessionId = c.Param("session_id")
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Proctoring{}
	p.SessionId = input.SessionId
	p.ProctorId = input.ProctorId

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//update protoring to database
	tsql := fmt.Sprintf(`UPDATE proctoring SET session_id = '%d',  proctor_id  = '%d' WHERE proctoring.session_id = '%s'`, p.SessionId, p.ProctorId, sessionId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Update Proctoring Success"})
}

func ProctoringDelete(c *gin.Context) {
	var input ProctoringInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	p := models.Proctoring{}
	p.SessionId = input.SessionId
	p.ProctorId = input.ProctorId

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//insert report to database
	tsql := fmt.Sprintf(`DELETE FROM proctoring WHERE session_id= '%d' AND proctor_id='%d'`, p.SessionId, p.ProctorId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Delete Proctoring Success"})

}
