package controllers

import (
	"database/sql"
	"fmt"
	"go-nuol-api/models"
	"html"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

//#Query ສູນສອບ
func GetSites(c *gin.Context) {
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := `select ex.id, ex.site_name, cg.name as group_name, (select count(er.id) from lums_data.sis_exam_room as er right join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) where eb.exam_site_id=ex.id) as room_no, (select count(af.id) from lums_data.sis_application_form as af right join lums_data.sis_exam_seat as es on (af.seat_no=es.seat_no) right join lums_data.sis_exam_room as err on (es.exam_room_id=err.id) right join lums_data.sis_exam_building as ebb on (err.exam_building_id=ebb.id) where ebb.exam_site_id=ex.id) as applicant_no from lums_data.sis_exam_site as ex left join lums_data.sis_curriculum_group as cg on (ex.curriculum_group_id=cg.id) where ex.active=1 and cg.active=1`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sitersl []models.Sites
	for rows.Next() {
		site := models.Sites{}
		rows.Scan(&site.Id, &site.SiteName, &site.GroupName, &site.RoomNo, &site.ApplicantNo)
		sitersl = append(sitersl, site)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": sitersl})
}

//Query ຫ້ອງສອບຂອງສູນສອບ {$exam_site_id}
func GetSiteExamRooms(c *gin.Context) {
	var exam_site_id = c.Param("exam_site_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`select er.id, er.room_name from lums_data.sis_exam_room as er where er.exam_building_id in (select eb.id from lums_data.sis_exam_building as eb where eb.exam_site_id='%s')`, exam_site_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var siteexamroomsrsl []models.SiteExamRooms
	for rows.Next() {
		siteexamrooms := models.SiteExamRooms{}
		rows.Scan(&siteexamrooms.Id, &siteexamrooms.Room_name)
		siteexamroomsrsl = append(siteexamroomsrsl, siteexamrooms)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": siteexamroomsrsl})

}

//Query ລາຍການຫ້ອງສອບ ທີ່ຂຶ້ນກັບກໍາມະການ {$proctor_id}
func GetRoomProctors(c *gin.Context) {
	var proctor_id = c.Param("proctor_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := fmt.Sprintf(`select er.id, er.room_name, eb.building_name, ex.site_name from lums_data.sis_exam_room as er left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) right join foe.session as se on (se.room_id=er.id) right join foe.proctoring as pg on (se.session_id=pg.session_id) where pg.proctor_id='%s' group by se.room_id`, proctor_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var roomproctocrrsl []models.RoomProctors
	for rows.Next() {
		roomproctors := models.RoomProctors{}
		rows.Scan(&roomproctors.Id, &roomproctors.RoomName, &roomproctors.BuildingName, &roomproctors.SiteName)
		roomproctocrrsl = append(roomproctocrrsl, roomproctors)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": roomproctocrrsl})

}

//Query session (ພາກ) ສອບ ທີ່ຂຶ້ນກັບຫ້ອງ {$exam_room_id}
func GetSessionRooms(c *gin.Context) {
	var exam_site_id = c.Param("exam_site_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := fmt.Sprintf(`select se.*, er.room_name, eb.building_name, ex.site_name from foe.session as se left join lums_data.sis_exam_room as er on (se.room_id=er.id) left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) right join foe.proctoring as pg on (se.session_id=pg.session_id) where se.room_id='%s' group by se.session_id`, exam_site_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sessionroomsrsl []models.ExamSession
	for rows.Next() {
		sessionrooms := models.ExamSession{}
		rows.Scan(&sessionrooms.SessionId, &sessionrooms.RoomId, &sessionrooms.Date, &sessionrooms.Session, &sessionrooms.Status, &sessionrooms.RoomName, &sessionrooms.BuildingName, &sessionrooms.SiteName)
		sessionroomsrsl = append(sessionroomsrsl, sessionrooms)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": sessionroomsrsl})
}

//Query session (ພາກ) ສອບ ທີ່ຂຶ້ນກໍາມະການ {$proctor_id}
func GetSessionProctors(c *gin.Context) {
	var proctor_id = c.Param("proctor_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//insert protoring to database
	tsql := fmt.Sprintf(`select se.*, er.room_name, eb.building_name, ex.site_name from foe.session as se left join lums_data.sis_exam_room as er on (se.room_id=er.id) left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) right join foe.proctoring as pg on (se.session_id=pg.session_id) where pg.proctor_id='%s'`, proctor_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sessionproctorsrsl []models.SessionProctors
	for rows.Next() {
		sessionproctor := models.SessionProctors{}
		rows.Scan(&sessionproctor.SessionId, &sessionproctor.RoomId, &sessionproctor.Date, &sessionproctor.Session, &sessionproctor.Status, &sessionproctor.RoomName, &sessionproctor.BuildingName, &sessionproctor.SiteName)
		sessionproctorsrsl = append(sessionproctorsrsl, sessionproctor)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": sessionproctorsrsl})
}

//Query ນັກສອບເສັງທັງໝົດຂອງຫ້ອງ {$exam_room_id}
func GetStudentRoom(c *gin.Context) {
	var exam_room_id = c.Param("exam_room_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af.picture_file, af.mobile_no, af.province_name, if(af.school_name is null or af.school_name='', gs.school_name, af.school_name ) as school_name from lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_graduated_school as gs on (af.graduated_school_id=gs.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) where af.applicant_status_id=1 and er.id='%s' af.seat_no`, exam_room_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var studentroomrsl []models.StudentRoom
	for rows.Next() {
		studentroom := models.StudentRoom{}
		rows.Scan(&studentroom.Id, &studentroom.SeatNo, &studentroom.FullName, &studentroom.BirthDate, &studentroom.PictureFile, &studentroom.MobileNo, &studentroom.ProvinceName, &studentroom.SchoolName)
		studentroomrsl = append(studentroomrsl, studentroom)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": studentroomrsl})
}

//Query ຈໍານວນນັກສອບເສັງທັງໝົດຂອງຫ້ອງ {$exam_room_id}
func GetCountStudent(c *gin.Context) {
	var exam_room_id = c.Param("exam_room_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select  count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) where af.applicant_status_id=1 and er.id='%s'`, exam_room_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var countstudentrsl []models.CountStudent
	for rows.Next() {
		countstudent := models.CountStudent{}
		rows.Scan(&countstudent.Count)
		countstudentrsl = append(countstudentrsl, countstudent)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": countstudentrsl, "room_id": exam_room_id})
}

//Query ຈໍານວນນັກສອບເສັງເຂົ້າຫ້ອງເສັງ (session) {$session_id}
func GetCountStudentSession(c *gin.Context) {
	var session_id = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select  count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) where af.applicant_status_id=1 and er.id='%s'`, session_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var countstudentsessionrsl []models.CountStudentSession
	for rows.Next() {
		countstudentsession := models.CountStudentSession{}
		rows.Scan(&countstudentsession.Count)
		countstudentsessionrsl = append(countstudentsessionrsl, countstudentsession)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": countstudentsessionrsl, "session_id": session_id})

}

//Query ຈໍານວນນັກສອບຂາດສອບເສັງຂອງ session {$session_id}
func GetCountStudentAbsent(c *gin.Context) {
	var session_id = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on (af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join foe.session as se on (er.id=se.room_id) where af.applicant_status_id=1 and se.session_id='%s' and af.id not in (select att.applicant_id from foe.attendance as att left join foe.session as se on (att.session_id=se.session_id) where se.session_id='%s');`, session_id, session_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var countstudentabsentrsl []models.CountStudentAbsent
	for rows.Next() {
		countstudentabsent := models.CountStudentAbsent{}
		rows.Scan(&countstudentabsent.Count)
		countstudentabsentrsl = append(countstudentabsentrsl, countstudentabsent)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": countstudentabsentrsl, "session_id": session_id})
}

//Query ຂໍ້ມູນນັກສອບເສັງ {$applicant_id}
func GetStudentDetail(c *gin.Context) {
	var applicant_id = c.Param("applicant_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select  af.id, af.applicant_code, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af. mobile_no, af.province_name, school_name, af.seat_no, er.room_name, eb.building_name, ex.site_name from lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) where af.applicant_status_id=1 and af.id='%s';`, applicant_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var studentdetailrsl []models.StudentDetail
	for rows.Next() {
		studentdetail := models.StudentDetail{}
		rows.Scan(&studentdetail.Id, &studentdetail.ApplicantCode, &studentdetail.FullName, &studentdetail.BirthDate, &studentdetail.MobileNo, &studentdetail.ProvinceName, &studentdetail.SchoolName, &studentdetail.SeatNumber, &studentdetail.RoomName, &studentdetail.BuildingName, &studentdetail.SiteName)
		studentdetailrsl = append(studentdetailrsl, studentdetail)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": studentdetailrsl})
}

//Query ຂໍ້ມູນກໍາມະການຍາມຫ້ອງສອບ {$proctor_id}
func GetProctorDetail(c *gin.Context) {
	var proctor_id = c.Param("proctor_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select pt.proctor_id, pt.name, pt.from_faculty, pt.username, rol.role_name from foe.proctor as pt left join foe.role as rol on (pt.role_id=rol.role_id) where pt.username!='admin' and pt.proctor_id='%s'`, proctor_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var proctordetailrsl []models.ProctorDetail
	for rows.Next() {
		proctordetail := models.ProctorDetail{}
		rows.Scan(&proctordetail.Id, &proctordetail.Name, &proctordetail.FromFaculty, &proctordetail.Username, &proctordetail.RoleName)
		proctordetailrsl = append(proctordetailrsl, proctordetail)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": proctordetailrsl})
}

//Query ຂໍ້ມູນລາຍງານສະຖິຕິນັກສອບເສັງທັງໝົດ ແລະ ມາເຂົ້າສອບໃນແຕ່ລະຫ້ອງ-session ຂອງສູນສອບ {$exam_site_id}
func GetSiteStatus(c *gin.Context) {
	type SiteStatusInput struct {
		ExamRoomId int    `json:"room_id"`
		Date       string `json:"date"`
		Session    int    `json:"session_id"`
	}
	var input SiteStatusInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select count(af.id) as total, (select count(*) from foe.attendance as att where att.session_id=se.session_id) as attendance, er.room_name from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on (af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join foe.session as se on (er.id=se.room_id) where af.applicant_status_id=1 and er.id='%d' and se.date='%s' and se.session='%d';`, input.ExamRoomId, input.Date, input.Session)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sitedetailrsl []models.SiteStatus
	for rows.Next() {
		sitedetail := models.SiteStatus{}
		rows.Scan(&sitedetail.Total, &sitedetail.Attendance, &sitedetail.RoomName)
		sitedetailrsl = append(sitedetailrsl, sitedetail)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": sitedetailrsl})
}

//Query ສະແດງຂໍ້ມູນສະຫຼຸບໃນລາຍງານຫ້ອງ-session ສອບເສັງ: ຂໍ້ມູນຫ້ອງ-session, ຈໍານວນ ແລະ ລາຍຊື່ຜູ້ເຮັດຜິດລະບຽບ (ຕັກເຕືອນ|ຕັດສິດ), ລາຍຊື່ຄົນຂາດ return json {ຈໍານວນນັກສອບເສັງທັງທົດ, ຈໍານວນນັກສອບເສັງຂາດ, ຈໍານວນນັກສອບເສັງຜູ້ຜິດລະບຽບ {ຕັກເຕືອນ, ຕັດສິດ} }

//ຈໍານວນນັກສອບເສັງທັງທົດ-session
func GetReportbySession(c *gin.Context) {
	var session_id = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select  count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join foe.session as se on (er.id=se.room_id) where af.applicant_status_id=1 and se.session_id='%s'`, session_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var countstudentbysessionrsl []models.CountStudentBySession
	for rows.Next() {
		countstudentbysession := models.CountStudentBySession{}
		rows.Scan(&countstudentbysession.Count)
		countstudentbysessionrsl = append(countstudentbysessionrsl, countstudentbysession)
	}

	tsql_2 := fmt.Sprintf(`select count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on (af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join foe.session as se on (er.id=se.room_id) where af.applicant_status_id=1 and se.session_id='%s' and af.id not in (select att.applicant_id from foe.attendance as att left join foe.session as se on (att.session_id=se.session_id) where se.session_id='%s');`, session_id, session_id)

	rows2, err := db.Query(tsql_2)
	if err != nil {
		panic(err.Error())
	}

	var countstudentabsentrsl []models.CountStudentAbsent
	for rows2.Next() {
		countstudentabsent := models.CountStudentAbsent{}
		rows2.Scan(&countstudentabsent.Count)
		countstudentabsentrsl = append(countstudentabsentrsl, countstudentabsent)
	}

	tsql_3 := fmt.Sprintf(`select count(DISTINCT rb.applicant_id) from foe.rule_breaker as rb where rb.penalty=0 and rb.session_id='%s'`, session_id)

	rows3, err := db.Query(tsql_3)
	if err != nil {
		panic(err.Error())
	}

	var countstudentsoftpenaltyrsl []models.CountStudentSoftPenalty
	for rows3.Next() {
		countstudentsoftpenalty := models.CountStudentSoftPenalty{}
		rows3.Scan(&countstudentsoftpenalty.Count)
		countstudentsoftpenaltyrsl = append(countstudentsoftpenaltyrsl, countstudentsoftpenalty)
	}

	tsql_4 := fmt.Sprintf(`select count(DISTINCT rb.applicant_id) from foe.rule_breaker as rb where rb.penalty=1 and rb.session_id='%s'`, session_id)

	rows4, err := db.Query(tsql_4)
	if err != nil {
		panic(err.Error())
	}

	var countstudenthardpenaltyrsl []models.CountStudentHardPenalty
	for rows4.Next() {
		countstudenthardpenalty := models.CountStudentHardPenalty{}
		rows4.Scan(&countstudenthardpenalty.Count)
		countstudenthardpenaltyrsl = append(countstudenthardpenaltyrsl, countstudenthardpenalty)
	}

	tsql_5 := fmt.Sprintf(`select af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, if(rb.penalty=1, 'ຕັດສິດສອບເສັງ', 'ຕັກເຕືອນ') as penalty from foe.rule_breaker as rb left join lums_data.sis_application_form as af on (rb.applicant_id=af.id) left join lums_data.sis_title as t on (af.title_id=t.id) left join foe.session as se on (rb.session_id=se.session_id) where rb.session_id='%s' group by rb.applicant_id order by rb.created_date`, session_id)

	rows5, err := db.Query(tsql_5)
	if err != nil {
		panic(err.Error())
	}

	var listrulebreakerrsl []models.ListRuleBreaker
	for rows5.Next() {
		listrulebreaker := models.ListRuleBreaker{}
		rows5.Scan(&listrulebreaker.SeatNumber, &listrulebreaker.FullName, &listrulebreaker.Penalty)
		listrulebreakerrsl = append(listrulebreakerrsl, listrulebreaker)
	}
	defer rows.Close()
	defer rows2.Close()
	defer rows3.Close()
	defer rows4.Close()
	defer rows5.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "ຈຳນວນທັງຫມົດນັກສອບເສັງທັງຫມົດ": countstudentbysessionrsl, "ຈຳນວນນັກສອບເສັງຂາດ": countstudentabsentrsl, "ຕັກເຕືອນ": countstudentsoftpenaltyrsl, "ຕັດສິດ": countstudenthardpenaltyrsl, "ລາຍຊື່ຜູ້ຜິດລະບຽບ": listrulebreakerrsl})
}

// ນັກສອບເສັງທີ່ຍັງບໍ່ທັນເຂົ້າຫ້ອງເສັງ {$session_id}
func GetListStudentNotInSession(c *gin.Context) {
	var session_id = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af.picture_file, af.mobile_no, af.province_name, if(af.school_name is null or af.school_name='', gs.school_name, af.school_name ) as school_name from lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_graduated_school as gs on (af.graduated_school_id=gs.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) left join foe.session as se on (se.room_id=er.id) where af.applicant_status_id=1 and af.id not in (select att.applicant_id from foe.attendance as att where att.session_id='%s') and se.session_id='%s' order by af.seat_no`, session_id, session_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var liststudentsroomrsl []models.ListStudentsRoom
	for rows.Next() {
		liststudentsroom := models.ListStudentsRoom{}
		rows.Scan(&liststudentsroom.Id, &liststudentsroom.SeatNo, &liststudentsroom.FullName, &liststudentsroom.BirthDate, &liststudentsroom.PictureFile, &liststudentsroom.MobileNo, &liststudentsroom.ProvinceName, &liststudentsroom.SchoolName)
		liststudentsroomrsl = append(liststudentsroomrsl, liststudentsroom)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": liststudentsroomrsl})
}

//Query ນັກສອບຂາດສອບເສັງຂອງ session {$session_id}
func GetStudentSessionAbsent(c *gin.Context) {
	GetListStudentNotInSession(c)
}

//Query ນັກສອບເສັງເຂົ້າຫ້ອງເສັງ (session) {$session_id}
func GetListStudentInSession(c *gin.Context) {
	var session_id = c.Param("session_id")
	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	//Select student to database
	tsql := fmt.Sprintf(`select af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af.picture_file, af.mobile_no, af.province_name, if(af.school_name is null or af.school_name='', gs.school_name, af.school_name ) as school_name from foe.attendance as at left join lums_data.sis_application_form as af on (at.applicant_id=af.id) left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_graduated_school as gs on (af.graduated_school_id=gs.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join foe.session as se on (at.session_id=se.session_id) where se.session_id='%s' order by at.created_date`, session_id)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var studentsessionrsl []models.ListStudentsRoom
	for rows.Next() {
		studentsession := models.ListStudentsRoom{}
		rows.Scan(&studentsession.Id, &studentsession.SeatNo, &studentsession.FullName, &studentsession.BirthDate, &studentsession.PictureFile, &studentsession.MobileNo, &studentsession.ProvinceName, &studentsession.SchoolName)
		studentsessionrsl = append(studentsessionrsl, studentsession)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": studentsessionrsl})
}

//Query ຫ້ອງສອບຂອງສູນສອບ {$exam_site_id}
func GetSitesDetails(c *gin.Context) {
	var examsiteid = c.Param("exam_site_id")

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}

	tsql := fmt.Sprintf(`select ex.id, ex.site_name from lums_data.sis_exam_site as ex where id='%s';`, examsiteid)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var sitersl []models.SiteExam
	for rows.Next() {
		site := models.SiteExam{}
		rows.Scan(&site.Id, &site.SiteName)
		tsql2 := fmt.Sprintf(`select er.id, er.room_name from lums_data.sis_exam_room as er where er.exam_building_id in (select eb.id from lums_data.sis_exam_building as eb where eb.exam_site_id='%d');`, site.Id)
		rows2, err := db.Query(tsql2)
		if err != nil {
			panic(err.Error())
		}
		defer rows2.Close()
		var siterexamroomrsl []models.SiteExamRooms
		for rows2.Next() {
			siterexamroom := models.SiteExamRooms{}
			rows2.Scan(&siterexamroom.Id, &siterexamroom.Room_name)

			tsql3 := fmt.Sprintf(`select se.session_id, se.date, se.session, (select count(af.id) from lums_data.sis_application_form as af left join lums_data.sis_exam_seat as es on (af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) where er.id=se.room_id) as total_applicant, (select count(att.applicant_id) from foe.attendance as att where att.session_id=se.session_id) as attended_applicant from foe.session as se where se.room_id='%d'`, siterexamroom.Id)
			rows3, err := db.Query(tsql3)
			if err != nil {
				panic(err.Error())
			}
			defer rows3.Close()
			var sessiondetailrsl []models.ListSessionDetails
			for rows3.Next() {
				sessiondetail := models.ListSessionDetails{}
				rows3.Scan(&sessiondetail.SessionId, &sessiondetail.Date, &sessiondetail.Session, &sessiondetail.TotalApplicant, &sessiondetail.AttendanceApplicant)
				sessiondetailrsl = append(sessiondetailrsl, sessiondetail)
			}
			siterexamroom.Session = sessiondetailrsl
			siterexamroomrsl = append(siterexamroomrsl, siterexamroom)
		}
		site.Rooms = siterexamroomrsl
		sitersl = append(sitersl, site)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	defer db.Close()

	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "Data": sitersl})

}

// #Query ສະຖິຕິຈໍານວນນັກສອບເສັງ ແລະ ມາເຂົ້າສອບເສັງ ທັງໝົດ
func GetOveralStatistic(c *gin.Context) {
	DBURL := models.DbConnect()
	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := `select count(af.id) as total, (select count(DISTINCT applicant_id) from foe.attendance) as attendance from lums_data.sis_application_form as af where af.seat_no is not null`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var data []models.TotalAndAttendance
	for rows.Next() {
		resault := models.TotalAndAttendance{}
		rows.Scan(&resault.Total, &resault.Attendance)
		data = append(data, resault)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": data})
}

// #Query ລາຍການບັນຊີຜູ້ໃຊ້ທັງໝົດ
func GetListProctor(c *gin.Context) {
	DBURL := models.DbConnect()
	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := `select pt.proctor_id, pt.name, pt.from_faculty, pt.username, pt.role_id, rl.role_name from foe.proctor as pt left join foe.role as rl on (pt.role_id=rl.role_id)`
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var data []models.ListProctor
	for rows.Next() {
		resault := models.ListProctor{}
		rows.Scan(&resault.ProctorId, &resault.Name, &resault.FromFaculty, &resault.Username, &resault.RoleId, &resault.RoleName)

		tsql1 := fmt.Sprintf(`select er.id, er.room_name, eb.id, eb.building_name, xs.id, xs.site_name from foe.proctoring as pt left join foe.session as se on(pt.session_id=se.session_id) left join lums_data.sis_exam_room as er on(se.room_id=er.id) left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as xs on(eb.exam_site_id=xs.id) where pt.proctor_id=%d group by er.id`, resault.ProctorId)
		rows1, err := db.Query(tsql1)
		if err != nil {
			panic(err.Error())
		}
		for rows1.Next() {
			room := models.ProctorRoom{}
			rows1.Scan(&room.RoomId, &room.RoomName, &room.BuildingId, &room.BuildingName, &room.SiteId, &room.SiteId)
			resault.ProctorRoom = append(resault.ProctorRoom, room)
		}
		data = append(data, resault)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": data})
}

// #Query ຂໍ້ມູນສູນສອບ ແລະ session ທີ່ຂຶ້ນກັບ
func GetSitesSession(c *gin.Context) {
	var examsiteid = c.Param("exam_site_id")

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	tsql := fmt.Sprintf(`select ex.id, ex.site_name, ex.active from lums_data.sis_exam_site as ex where ex.id=%s`, examsiteid)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var data []models.SiteSession
	for rows.Next() {
		resault := models.SiteSession{}
		rows.Scan(&resault.SiteId, &resault.SiteName, &resault.SiteActive)

		tsql1 := fmt.Sprintf(`select se.date, se.session from foe.session as se left join lums_data.sis_exam_room as er on (se.room_id=er.id) left join lums_data.sis_exam_building as eb on (eb.id=er.exam_building_id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) where ex.id=%s group by se.date, se.session order by se.date, session desc`, examsiteid)
		rows1, err := db.Query(tsql1)
		if err != nil {
			panic(err.Error())
		}
		defer rows1.Close()
		var session []models.SessionList
		for rows1.Next() {
			resault1 := models.SessionList{}
			rows1.Scan(&resault1.Date, &resault1.Session)
			session = append(session, resault1)
		}
		resault.SessionList = session
		data = append(data, resault)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": data})
}

// #Query ສະຖິຕິນັກສອບເສັງທັງໝົດ ແລະ ມາເຂົ້າເສັງ ຂອງແຕ່ລະ session ທີ່ຂຶ້ນກັບສູນສອບ {$exam_site_id}, {$date}, {$session}
func GetSiteStatistic(c *gin.Context) {

	var ExamSiteId = c.Param("exam_site_id")
	var Date = c.Param("date")
	var Session = c.Param("session")

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`select ex.id, er.room_name, se.session_id, (select count(*) from lums_data.sis_application_form where seat_no in (select seat_no from lums_data.sis_exam_seat where exam_room_id=er.id) ) as total, (select count(*) from foe.attendance where session_id=se.session_id) as attendance from foe.session as se left join lums_data.sis_exam_room as er on (se.room_id=er.id) right join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) right join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) where ex.id=%s and se.date='%s' and se.session='%s'`, ExamSiteId, Date, Session)
	// println(tsql)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var data []models.ListSessionStatistic
	for rows.Next() {
		resault := models.ListSessionStatistic{}
		rows.Scan(&resault.SiteId, &resault.RoomName, &resault.SessionId, &resault.Total, &resault.Attendance)
		data = append(data, resault)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Get Success", "data": data})
}

// Add Event Log
func AddEventLog(c *gin.Context) {
	type logging struct {
		Event  string `json:"event"`
		UserId int    `json:"proctor_id"`
	}

	var input logging
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var Event = input.Event
	var UserId = input.UserId

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	//update protoring to database
	tsql := fmt.Sprintf(`insert into backend_log value (null, now(), '%s', %d)`, Event, UserId)
	insert, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer insert.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "logging Success"})
}

// Room Detail
func GetRoomDetails(c *gin.Context) {
	type Site struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
	}

	type Building struct {
		Id   int    `json:"id"`
		Name string `json:"name"`
		Site Site   `json:"site"`
	}

	type Applicant struct {
		Id           int     `json:"id"`
		SeatNo       string  `json:"seat_no"`
		FullName     string  `json:"full_name"`
		PictureFile  *string `json:"picture_file"`
		BirthDate    *string `json:"birth_date"`
		MobileNo     *string `json:"mobile_no"`
		TelphoneNo   *string `json:"telphone_no"`
		ProvinceName *string `json:"province_name"`
		SchoolName   *string `json:"school_name"`
	}

	type Proctor struct {
		Id          int     `json:"proctor_id"`
		Name        string  `json:"name"`
		FromFaculty *string `json:"from_faculty"`
	}

	type Session struct {
		Id          int         `json:"id"`
		Date        string      `json:"date"`
		Session     string      `json:"session"`
		Attendances []Applicant `json:"attendances"`
		Proctors    []Proctor   `json:"proctors"`
	}

	type RoomDetail struct {
		RoomId     int         `json:"room_id"`
		RoomName   string      `json:"room_name"`
		Building   Building    `json:"building"`
		Applicants []Applicant `json:"applicants"`
		Sessions   []Session   `json:"sessions"`
	}

	var RoomId = c.Param("room_id")
	RoomId = html.EscapeString(strings.TrimSpace(RoomId))

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	Data := RoomDetail{}
	tsql := fmt.Sprintf(`select er.id, er.room_name, er.exam_building_id, eb.building_name, eb.exam_site_id, ex.site_name from lums_data.sis_exam_room as er left join lums_data.sis_exam_building as eb on (er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on (eb.exam_site_id=ex.id) where er.id=%s`, RoomId)
	rows := db.QueryRow(tsql)
	rows.Scan(&Data.RoomId, &Data.RoomName, &Data.Building.Id, &Data.Building.Name, &Data.Building.Site.Id, &Data.Building.Site.Name)

	tsql1 := fmt.Sprintf(`select af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af.picture_file, af.mobile_no, af.province_name, if(af.school_name is null or af.school_name='', gs.school_name, af.school_name ) as school_name from lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_graduated_school as gs on (af.graduated_school_id=gs.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join lums_data.sis_exam_room as er on (es.exam_room_id=er.id) where af.applicant_status_id=1 and er.id=%s order by af.seat_no`, RoomId)
	rows1, err := db.Query(tsql1)
	if err != nil {
		fmt.Println(err.Error())
	}

	for rows1.Next() {
		r := Applicant{}
		rows1.Scan(&r.Id, &r.SeatNo, &r.FullName, &r.BirthDate, &r.PictureFile, &r.MobileNo, &r.ProvinceName, &r.SchoolName)
		Data.Applicants = append(Data.Applicants, r)
	}
	rows1.Close()

	tsql2 := fmt.Sprintf(`select session_id, session, date from foe.session where room_id=%s`, RoomId)
	rows2, err := db.Query(tsql2)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for rows2.Next() {
			r := Session{}
			rows2.Scan(&r.Id, &r.Session, &r.Date)

			tsql3 := fmt.Sprintf(`select af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name, af.birth_date, af.picture_file, af.mobile_no, af.province_name, if(af.school_name is null or af.school_name='', gs.school_name, af.school_name ) as school_name from foe.attendance as at left join lums_data.sis_application_form as af on (at.applicant_id=af.id) left join lums_data.sis_title as t on (af.title_id=t.id) left join lums_data.sis_graduated_school as gs on (af.graduated_school_id=gs.id) left join lums_data.sis_exam_seat as es on ( af.seat_no=es.seat_no) left join foe.session as se on (at.session_id=se.session_id) where se.session_id=%d order by af.seat_no`, r.Id)
			rows3, err := db.Query(tsql3)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				for rows3.Next() {
					a := Applicant{}
					rows3.Scan(&a.Id, &a.SeatNo, &a.FullName, &a.BirthDate, &a.PictureFile, &a.MobileNo, &a.ProvinceName, &a.SchoolName)
					r.Attendances = append(r.Attendances, a)
				}
			}
			rows3.Close()

			tsql4 := fmt.Sprintf(`select p.proctor_id, p.Name, p.from_faculty from foe.proctor as p right join foe.proctoring as pr on(p.proctor_id=pr.proctor_id) where pr.session_id=%d`, r.Id)
			rows4, err := db.Query(tsql4)
			if err != nil {
				fmt.Println(err)
			}

			for rows4.Next() {
				p := Proctor{}
				rows4.Scan(&p.Id, &p.Name, &p.FromFaculty)
				r.Proctors = append(r.Proctors, p)
			}
			rows4.Close()

			Data.Sessions = append(Data.Sessions, r)
		}
	}
	rows2.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"data": Data, "message": "Success"})

}

// Get Curriculum Groups
func GetCurriculumGroup(c *gin.Context) {
	type CurriculumGroup struct {
		Id      int    `json:"id"`
		GroupNo string `json:"group_no"`
		Name    string `json:"name"`
	}

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var Data []CurriculumGroup
	tsql := "SELECT id, group_no, name FROM lums_data.sis_curriculum_group WHERE active=1"
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		cg := CurriculumGroup{}
		rows.Scan(&cg.Id, &cg.GroupNo, &cg.Name)
		Data = append(Data, cg)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"data": Data, "message": "Success"})
}

// Get Site by Curriculum Group
func GetSiteByGroup(c *gin.Context) {
	type SiteList struct {
		Id       int    `json:"id"`
		SiteName string `json:"site_name"`
	}

	type CurriculumGroup struct {
		GroupId   int        `json:"id"`
		GroupNo   string     `json:"group_no"`
		GroupName string     `json:"name"`
		Sites     []SiteList `json:"sites"`
	}

	var GroupId = c.Param("group_id")
	GroupId = html.EscapeString(strings.TrimSpace(GroupId))

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var Data CurriculumGroup
	tsql := fmt.Sprintf("SELECT id, group_no, name FROM lums_data.sis_curriculum_group WHERE id=%s", GroupId)
	rows := db.QueryRow(tsql)
	rows.Scan(&Data.GroupId, &Data.GroupNo, &Data.GroupName)

	tsql1 := fmt.Sprintf(`SELECT id, site_name FROM lums_data.sis_exam_site WHERE active=1 and curriculum_group_id=%s`, GroupId)
	rows1, err := db.Query(tsql1)
	if err != nil {
		panic(err.Error())
	}
	defer rows1.Close()

	for rows1.Next() {
		s := SiteList{}
		rows1.Scan(&s.Id, &s.SiteName)
		Data.Sites = append(Data.Sites, s)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"data": Data, "message": "Success"})
}

// Get room by site
func GetRoomBySite(c *gin.Context) {
	type SessionList struct {
		Id      int    `json:"id"`
		Date    string `json:"date"`
		Session string `json:"session"`
		Status  string `json:"status"`
	}

	type RoomList struct {
		Id       int           `json:"id"`
		RoomName string        `json:"room_name"`
		Sessions []SessionList `json:"sessions"`
	}

	type SiteList struct {
		SiteId   int        `json:"id"`
		SiteName string     `json:"site_name"`
		Rooms    []RoomList `json:"rooms"`
	}

	type CurriculumGroup struct {
		GroupId   int      `json:"id"`
		GroupNo   string   `json:"group_no"`
		GroupName string   `json:"name"`
		Site      SiteList `json:"site"`
	}

	var SiteId = c.Param("site_id")
	SiteId = html.EscapeString(strings.TrimSpace(SiteId))

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var Data CurriculumGroup
	tsql := fmt.Sprintf(`SELECT ex.id, ex.site_name, cg.id as group_id, cg.group_no, cg.name as group_name FROM lums_data.sis_exam_site as ex left join lums_data.sis_curriculum_group as cg on(ex.curriculum_group_id=cg.id) WHERE ex.id=%s`, SiteId)
	rows := db.QueryRow(tsql)
	rows.Scan(&Data.Site.SiteId, &Data.Site.SiteName, &Data.GroupId, &Data.GroupNo, &Data.GroupName)

	tsql1 := fmt.Sprintf(`SELECT er.id, er.room_name FROM lums_data.sis_exam_room as er left join lums_data.sis_exam_building as eb on(er.exam_building_id=eb.id) WHERE eb.exam_site_id=%s`, SiteId)
	rows1, err := db.Query(tsql1)
	if err != nil {
		panic(err.Error())
	}
	defer rows1.Close()
	for rows1.Next() {
		r := RoomList{}
		rows1.Scan(&r.Id, &r.RoomName)

		tsql2 := fmt.Sprintf(`SELECT session_id, date, session, status FROM foe.session WHERE room_id=%d`, r.Id)
		// println(tsql2)
		rows2, err := db.Query(tsql2)
		if err != nil {
			fmt.Println(err.Error())
		}
		defer rows2.Close()
		for rows2.Next() {
			se := SessionList{}
			rows2.Scan(&se.Id, &se.Date, &se.Session, &se.Status)
			r.Sessions = append(r.Sessions, se)
		}

		Data.Site.Rooms = append(Data.Site.Rooms, r)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"data": Data, "message": "Success"})
}

// Get Final report of a room
func GetReportRoom(c *gin.Context) {

	type FaultLog struct {
		Text    string `json:"text"`
		Penalty string `json:"penalty"`
		Date    string `json:"date"`
	}

	type RuleBreaker struct {
		Id       int        `json:"id"`
		FullName string     `json:"full_name"`
		SeatNo   string     `json:"seat_no"`
		Fault    []FaultLog `json:"fault"`
	}

	type Absent struct {
		Id       int    `json:"id"`
		FullName string `json:"full_name"`
		SeatNo   string `json:"seat_no"`
	}

	type Proctor struct {
		Id   int    `json:"proctor_id"`
		Name string `json:"proctor_name"`
	}

	type Report struct {
		Id            int           `json:"id"`
		Detail        string        `json:"detail"`
		CreatedDate   string        `json:"CreateDate"`
		SessionId     int           `json:"session_id"`
		Session       string        `json:"session"`
		SessionDate   string        `json:"session_date"`
		SessionStatus string        `json:"session_status"`
		RoomName      string        `json:"room_name"`
		Building      string        `json:"building"`
		SiteId        string        `json:"site_id"`
		SiteName      string        `json:"site_name"`
		GroupId       string        `json:"group_id"`
		GroupName     string        `json:"group_name"`
		Proctors      []Proctor     `json:"proctors"`
		ApplicantNo   int           `json:"applicant_no"`
		Absent        []Absent      `json:"absent"`
		RuleBreaker   []RuleBreaker `json:"rule_breaker"`
	}

	var SessionId = c.Param("session_id")
	SessionId = html.EscapeString(strings.TrimSpace(SessionId))

	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var Data Report
	tsql := fmt.Sprintf(`select se.session_id, se.date, se.session, se.status, er.room_name, eb.building_name, ex.id, ex.site_name, cg.id, cg.name FROM foe.session as se left join lums_data.sis_exam_room as er on(se.room_id=er.id) left join lums_data.sis_exam_building as eb on(er.exam_building_id=eb.id) left join lums_data.sis_exam_site as ex on(eb.exam_site_id=ex.id) left join lums_data.sis_curriculum_group as cg on(ex.curriculum_group_id=cg.id) WHERE se.session_id=%s`, SessionId)
	rows := db.QueryRow(tsql)
	rows.Scan(&Data.SessionId, &Data.SessionDate, &Data.Session, &Data.SessionStatus, &Data.RoomName, &Data.Building, &Data.SiteId, &Data.SiteName, &Data.GroupId, &Data.GroupName)

	tsql = fmt.Sprintf(`SELECT report_id, detail, created_date FROM foe.report WHERE session_id=%s`, SessionId)
	rows = db.QueryRow(tsql)
	rows.Scan(&Data.Id, &Data.Detail, &Data.CreatedDate)

	tsql = fmt.Sprintf(`SELECT count(af.id) FROM lums_data.sis_application_form as af right join lums_data.sis_exam_seat as es on(af.seat_no=es.seat_no) right join lums_data.sis_exam_room as er on(es.exam_room_id=er.id) right join foe.session as se on(er.id=se.room_id) WHERE se.session_id=%s`, SessionId)
	rows = db.QueryRow(tsql)
	rows.Scan(&Data.ApplicantNo)

	tsql = fmt.Sprintf(`SELECT pt.proctor_id, pt.name FROM foe.proctor as pt right join foe.proctoring as pg on(pt.proctor_id=pg.proctor_id) WHERE pg.session_id=%s`, SessionId)
	rows1, err := db.Query(tsql)
	if err == nil {
		for rows1.Next() {
			p := Proctor{}
			rows1.Scan(&p.Id, &p.Name)
			Data.Proctors = append(Data.Proctors, p)
		}
	}
	defer rows1.Close()

	tsql = fmt.Sprintf(`SELECT af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name FROM lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) right join lums_data.sis_exam_seat as es on(af.seat_no=es.seat_no) right join lums_data.sis_exam_room as er on(es.exam_room_id=er.id) right join foe.session as se on(er.id=se.room_id) WHERE af.id not in (select att.applicant_id from foe.attendance as att where att.session_id=%s ) and se.session_id=%s order by af.seat_no`, SessionId, SessionId)
	rows2, err := db.Query(tsql)
	if err == nil {
		for rows2.Next() {
			abs := Absent{}
			rows2.Scan(&abs.Id, &abs.SeatNo, &abs.FullName)
			Data.Absent = append(Data.Absent, abs)
		}
	}
	defer rows2.Close()

	tsql = fmt.Sprintf(`SELECT af.id, af.seat_no, concat_ws(' ', t.title_description, af.first_name, af.last_name) as full_name FROM lums_data.sis_application_form as af left join lums_data.sis_title as t on (af.title_id=t.id) right join lums_data.sis_exam_seat as es on(af.seat_no=es.seat_no) right join lums_data.sis_exam_room as er on(es.exam_room_id=er.id) right join foe.session as se on(er.id=se.room_id) WHERE af.id in (select rk.applicant_id from foe.rule_breaker as rk where rk.session_id=%s ) and se.session_id=%s order by af.seat_no`, SessionId, SessionId)
	rows3, err := db.Query(tsql)
	if err == nil {
		for rows3.Next() {
			rk := RuleBreaker{}
			rows3.Scan(&rk.Id, &rk.SeatNo, &rk.FullName)

			tsql = fmt.Sprintf(`SELECT fault, if(penalty, 'ຖືກຕັກສິດ', 'ຖືກຕັກເຕືອນ'), created_date FROM foe.rule_breaker WHERE applicant_id=%d and session_id=%s`, rk.Id, SessionId)
			rows4, err := db.Query(tsql)
			if err == nil {
				for rows4.Next() {
					f := FaultLog{}
					rows4.Scan(&f.Text, &f.Penalty, &f.Date)
					rk.Fault = append(rk.Fault, f)
				}
			}
			defer rows4.Close()
			Data.RuleBreaker = append(Data.RuleBreaker, rk)
		}
	}
	defer rows3.Close()

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"data": Data, "message": "Success"})
}

func GetBuildingBySite(c *gin.Context) {
	var SiteId = c.Param("site_id")

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`SELECT * FROM lums_data.sis_exam_building WHERE active=1 and exam_site_id=%s`, SiteId)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()
	var Data []models.Building
	for rows.Next() {
		building := models.Building{}
		rows.Scan(&building.Id, &building.SiteId, &building.BuildingCode, &building.BuildingName, &building.Active, &building.SetOrder, &building.LatLong)
		Data = append(Data, building)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//respond when is successful
	c.JSON(http.StatusOK, gin.H{"message": "Get Report Success", "data": Data})

}

func GetRoomByBuilding(c *gin.Context) {
	var BuildingId = c.Param("building_id")
	if BuildingId == "" {
		panic("Error! request")
	}

	//DB Connect
	DBURL := models.DbConnect()

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`SELECT * FROM lums_data.sis_exam_room WHERE active=1 and exam_building_id=%s`, BuildingId)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	var Data []models.Room
	for rows.Next() {
		room := models.Room{}
		rows.Scan(&room.Id, &room.BuildingId, &room.RoomName, &room.RoomSeat, &room.Active, &room.SetOrder)
		Data = append(Data, room)
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Get Report Success", "data": Data})
}
