package main

import (
	"go-nuol-api/controllers"
	"go-nuol-api/middlewares"

	// "github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	// r.Use(cors.New(cors.Config{
	// 	AllowOrigins: []string{"*"},
	// 	AllowMethods: []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
	// 	AllowHeaders: []string{"Content-Type,access-control-allow-origin, access-control-allow-headers"},
	// }))

	public := r.Group("/api")

	public.POST("/register", controllers.Register)
	public.POST("/login", controllers.Login)

	//Extra Endpoint
	public.GET("/sites", controllers.GetSites)
	public.GET("/siterooms/:exam_site_id", controllers.GetSiteExamRooms)
	public.GET("/building/:site_id", controllers.GetBuildingBySite)

	public.GET("/room/:building_id", controllers.GetRoomByBuilding)
	public.GET("/roomproctors/:proctor_id", controllers.GetRoomProctors)

	public.GET("/sessionroom/:exam_site_id", controllers.GetSessionRooms)
	public.GET("/sessionproctors/:proctor_id", controllers.GetSessionProctors)
	public.GET("/studentroom/:exam_room_id", controllers.GetStudentRoom)
	public.GET("/countstudent/:exam_room_id", controllers.GetCountStudent)
	public.GET("/countstudentsession/:session_id", controllers.GetCountStudentSession)
	public.GET("/countstudentabsent/:session_id", controllers.GetCountStudentAbsent)
	public.GET("/studentdetail/:applicant_id", controllers.GetStudentDetail)
	public.GET("/proctordetail/:proctor_id", controllers.GetProctorDetail)
	public.GET("/sitestatus", controllers.GetSiteStatus)
	public.GET("/overalstatistic", controllers.GetOveralStatistic)
	public.GET("/listproctors", controllers.GetListProctor)

	//Add New Extra
	public.GET("/reportstudentbysession/:session_id", controllers.GetReportbySession)
	public.GET("/liststudentsnotinsession/:session_id", controllers.GetListStudentNotInSession)
	public.GET("/liststudentsessionabsent/:session_id", controllers.GetStudentSessionAbsent)
	public.GET("/liststudentinsession/:session_id", controllers.GetListStudentInSession)
	public.GET("/sitedetails/:exam_site_id", controllers.GetSitesDetails)
	public.GET("/roomdetail/:room_id", controllers.GetRoomDetails)
	public.GET("/sitesession/:exam_site_id", controllers.GetSitesSession)
	public.GET("/sitestatistic/:exam_site_id/:date/:session", controllers.GetSiteStatistic)

	// Admin Report
	public.GET("/curriculumgroups", controllers.GetCurriculumGroup)
	public.GET("/sitebygroup/:group_id", controllers.GetSiteByGroup)
	public.GET("/roombysite/:site_id", controllers.GetRoomBySite)
	public.GET("/reportroom/:session_id", controllers.GetReportRoom)

	// Log
	public.POST("/logging", controllers.AddEventLog)

	//Default New Extra
	protected := r.Group("/api/proctor")
	protected.Use(middlewares.JwtAuthMiddleware())
	protected.GET("/user", controllers.CurrentUser)
	protected.PUT("/update", controllers.ProctorUpdate)
	protected.DELETE("/delete/:proctor_id", controllers.ProctorDelete)
	protected.POST("/import", controllers.ImportProctors)

	protected.GET("/proctoring", controllers.ProctoringRead)
	protected.POST("/proctoring", controllers.ProctoringCreat)
	protected.PUT("/proctoring/:session_id", controllers.ProctoringUpdate)
	protected.DELETE("/proctoring", controllers.ProctoringDelete)

	protected.GET("/rulebreaker", controllers.RuleBreakerRead)
	protected.POST("/rulebreaker", controllers.RuleBreakerCreate)
	protected.PUT("/rulebreaker/:applicant_id", controllers.RuleBreakerUpdate)
	protected.DELETE("/rulebreaker/:applicant_id", controllers.RuleBreakerDelete)

	protected.POST("/session", controllers.SessionCreate)
	protected.GET("/session", controllers.SessionRead)
	protected.PUT("/session/:session_id", controllers.SessionUpdate)
	protected.DELETE("/session/:session_id", controllers.SessionDelete)

	protected.GET("/report/all", controllers.GetReportReadAll)
	protected.GET("/report/:report_id", controllers.GetReportRead)
	protected.GET("/report/session/:session_id", controllers.GetReportReadBySession)
	protected.POST("/report", controllers.ReportCreate)
	protected.PUT("/report/:report_id", controllers.ReportUpdate)
	protected.DELETE("/report/:report_id", controllers.ReportDelete)

	protected.GET("/attendance", controllers.AttendanceRead)
	protected.POST("/attendance", controllers.AttendanceCreate)
	protected.PUT("/attendance/:applicant_id", controllers.AttendanceUpdate)
	protected.DELETE("/attendance", controllers.AttendanceDelete)

	protected2 := r.Group("/api/manager")
	protected2.Use(middlewares.JwtAuthMiddleware())
	protected2.GET("/role", controllers.RoleRead)
	protected2.POST("/role", controllers.RoleCreat)
	protected2.PUT("/role", controllers.RoleUpdate)
	protected2.DELETE("/role/:role_id", controllers.RoleDelete)

	r.Run(":8000")

}
