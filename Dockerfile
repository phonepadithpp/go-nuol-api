FROM golang:1.16-alpine

WORKDIR /app


COPY go.mod ./
COPY go.sum ./
COPY . .
RUN go mod download

COPY *.go ./

RUN go build -o /go-nuol-api

EXPOSE 8000

CMD [ "/go-nuol-api" ]