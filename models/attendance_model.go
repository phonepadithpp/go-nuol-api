package models

type Attendance struct {
	ApplicantId int    `json:"applicant_id"`
	SessionId   int    `json:"session_id"`
	CreateDate  string `json:"created_date"`
}
