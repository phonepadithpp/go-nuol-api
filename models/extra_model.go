package models

// import "golang.org/x/text/date"

type Site struct {
	Id          int    `json:"id"`
	SiteName    string `json:"site_name"`
	RoomNo      string `json:"room_no"`
	ApplicantNo string `json:"applicant_no"`
}

type Sites struct {
	Id          int    `json:"id"`
	SiteName    string `json:"site_name"`
	GroupName   string `json:"group_name"`
	RoomNo      string `json:"room_no"`
	ApplicantNo string `json:"applicant_no"`
}

type ListSessionDetails struct {
	SessionId           int    `json:"session_id"`
	Date                string `json:"date"`
	Session             string `json:"session"`
	TotalApplicant      string `json:"total_applicant"`
	AttendanceApplicant string `json:"attended_applicant"`
}

type SiteExamRooms struct {
	Id        int    `json:"room_id"`
	Room_name string `json:"room_name"`
	Session   []ListSessionDetails
}

type SiteExam struct {
	Id       int    `json:"id"`
	SiteName string `json:"site_name"`
	Rooms    []SiteExamRooms
}

type Building struct {
	Id           int    `json:"id"`
	BuildingCode string `json:"building_code"`
	BuildingName string `json:"building_Name"`
	Active       int    `json:"active"`
	SetOrder     int    `json:"set_order"`
	SiteId       string `json:"exam_site_id"`
	LatLong      string `json:"latlong"`
}

type Room struct {
	Id         int    `json:"id"`
	BuildingId int    `json:"building_id"`
	RoomName   string `json:"room_name"`
	RoomSeat   int    `json:"room_seat"`
	Active     int    `json:"active"`
	SetOrder   int    `json:"set_order"`
}

type RoomProctors struct {
	Id           int    `json:"id"`
	RoomName     string `json:"room_name"`
	BuildingName string `json:"building_name"`
	SiteName     string `json:"site_name"`
}

type ExamSession struct {
	SessionId    int    `json:"session_id"`
	RoomId       int    `json:"room_id"`
	Date         string `json:"date"`
	Session      string `json:"session"`
	Status       string `json:"status"`
	RoomName     string `json:"room_name"`
	BuildingName string `json:"building_name"`
	SiteName     string `json:"site_name"`
}

type SessionProctors struct {
	SessionId    int    `json:"session_id"`
	RoomId       int    `json:"room_id"`
	Date         string `json:"date"`
	Session      string `json:"session"`
	Status       string `json:"status"`
	RoomName     string `json:"room_name"`
	BuildingName string `json:"building_name"`
	SiteName     string `json:"site_name"`
}

type StudentRoom struct {
	Id           int     `json:"id"`
	SeatNo       string  `json:"seat_no"`
	FullName     string  `json:"full_name"`
	BirthDate    *string `json:"birth_date"`
	PictureFile  *string `json:"picture_file"`
	MobileNo     *string `json:"mobile_no"`
	ProvinceName *string `json:"province_name"`
	SchoolName   *string `json:"school_name"`
}

type StudentSession struct {
	Id            int    `json:"id"`
	ApplicantCode string `json:"applicant_code"`
	FullName      string `json:"full_name"`
}

type StudentSessionAbsent struct {
	Id            int    `json:"id"`
	ApplicantCode string `json:"applicant_code"`
	FullName      string `json:"full_name"`
}

type CountStudent struct {
	Count int `json:"count"`
}

type CountStudentSession struct {
	Count int `json:"count"`
}

type CountStudentAbsent struct {
	Count int `json:"count"`
}

type StudentDetail struct {
	Id            int     `json:"id"`
	ApplicantCode string  `json:"applicant_code"`
	FullName      string  `json:"full_name"`
	BirthDate     *string `json:"birth_date"`
	MobileNo      *string `json:"mobile_no"`
	ProvinceName  *string `json:"province_name"`
	SchoolName    *string `json:"school_name"`
	SeatNumber    string  `json:"seat_no"`
	RoomName      string  `json:"room_name"`
	BuildingName  string  `json:"building_name"`
	SiteName      string  `json:"site_name"`
}

type ProctorDetail struct {
	Id          int    `json:"proctor_id"`
	Name        string `json:"name"`
	FromFaculty string `json:"from_faculty"`
	Username    string `json:"username"`
	RoleName    string `json:"role_name"`
}

type SiteStatus struct {
	Total      int    `json:"total"`
	Attendance string `json:"attendance"`
	RoomName   string `json:"room_name"`
}

type CountStudentBySession struct {
	Count int `json:"count"`
}

type CountStudentSoftPenalty struct {
	Count int `json:"count"`
}

type CountStudentHardPenalty struct {
	Count int `json:"count"`
}

//#query ນັກສອບເສັງທັງໝົດຂອງຫ້ອງ

type ListStudentsRoom struct {
	Id           int     `json:"id"`
	SeatNo       string  `json:"seat_no"`
	FullName     string  `json:"full_name"`
	BirthDate    *string `json:"birth_date"`
	PictureFile  *string `json:"picture_file"`
	MobileNo     *string `json:"mobile_no"`
	ProvinceName *string `json:"province_name"`
	SchoolName   *string `json:"school_name"`
}

type ListRuleBreaker struct {
	SeatNumber string `json:"seat_no"`
	FullName   string `json:"full_name"`
	Penalty    string `json:"penalty"`
}

type TotalAndAttendance struct {
	Total      int    `json:"total"`
	Attendance string `json:"attendance"`
}

type ProctorRoom struct {
	RoomId       int    `json:"room_id"`
	RoomName     string `json:"room_name"`
	BuildingId   int    `json:"building_id"`
	BuildingName string `json:"building_name"`
	SiteId       int    `json:"site_id"`
	SiteName     string `json:"site_name"`
}

type ListProctor struct {
	ProctorId    int           `json:"proctor_id"`
	Name         string        `json:"name"`
	FromFaculty  *string       `json:"from_faculty"`
	Username     string        `json:"username"`
	Password     string        `json:"password"`
	RoleId       int           `json:"role_id"`
	RoleName     string        `json:"role_name"`
	ProctorRoom  []ProctorRoom `json:"proctor_room"`
	ModifiedDate string        `json:"modified_date"`
}

type SiteSession struct {
	SiteId      int           `json:"site_id"`
	SiteName    string        `json:"site_name"`
	SiteActive  int           `json:"site_active"`
	SessionList []SessionList `json:"site_session"`
}

type ListSessionStatistic struct {
	SiteId     int    `json:"site_id"`
	RoomName   string `json:"room_name"`
	SessionId  int    `json:"session_id"`
	Total      int    `json:"total"`
	Attendance int    `json:"attendance"`
}

type SessionList struct {
	Id      int    `json:"id"`
	Date    string `json:"date"`
	Session string `json:"session"`
	Status  string `json:"status"`
}
