package models

import (
	"database/sql"
	"fmt"

	"go-nuol-api/utils/token"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
)

type Proctor struct {
	ProctorId    int    `json:"proctor_id"`
	Name         string `json:"name"`
	FromFaculty  string `json:"from_faculty"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	RoleId       int    `json:"role_id"`
	RoleName     string `json:"role_name"`
	ModifiedDate string `json:"modified_date"`
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func GetUserByID(uid uint) (Proctor, error) {

	var u Proctor

	//load .env fie to connect database
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	DbHost := os.Getenv("DB_HOST")
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")
	DbPort := os.Getenv("DB_PORT")

	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	tsql := fmt.Sprintf(`SELECT proctor_id, name, if(from_faculty is null,'',from_faculty) as from_faculty, username, role_id, modified_date FROM proctor WHERE proctor_id = '%d'`, uid)
	rows, err := db.Query(tsql)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&u.ProctorId, &u.Name, &u.FromFaculty, &u.Username, &u.RoleId, &u.ModifiedDate)
		if err != nil {
			log.Fatalln(err)
		}
	}

	return u, nil

}

func LoginCheck(username string, password string) (string, error) {

	//load .env fie to connect database
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	DbHost := os.Getenv("DB_HOST")
	DbUser := os.Getenv("DB_USER")
	DbPassword := os.Getenv("DB_PASSWORD")
	DbName := os.Getenv("DB_NAME")
	DbPort := os.Getenv("DB_PORT")

	DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)

	db, err := sql.Open("mysql", DBURL)
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	var proctor Proctor

	tsql := fmt.Sprintf(`SELECT proctor_id, username, password, role_id, name FROM proctor WHERE username = '%s'`, username)
	rows, err := db.Query(tsql)
	if err != nil {
		//panic(err.Error())
		return "", err
	}
	defer rows.Close()

	if err != nil {
		return "", err
	}

	for rows.Next() {
		err = rows.Scan(&proctor.ProctorId, &proctor.Username, &proctor.Password, &proctor.RoleId, &proctor.Name)
		if err != nil {
			log.Fatalln(err)
		}

		if username == proctor.Username {
			fmt.Println("Found user")
			token, _ := token.GenerateToken(uint(proctor.ProctorId), proctor.RoleId, proctor.Username, proctor.Name)
			err = VerifyPassword(password, proctor.Password)

			if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
				return "", err
			}

			if err != nil {
				return "", err
			}

			return token, nil
		}

		if username != proctor.Username {
			fmt.Println("Not Found user")
			return "", err
		}

		log.Println(proctor.Username)
		log.Println(proctor.ProctorId)

	}

	return "", err

}
