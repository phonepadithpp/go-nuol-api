package models

type Role struct {
	RoleId   int    `json:"role_id"`
	RoleName string `json:"role_name"`
}
