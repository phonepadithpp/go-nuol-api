package models

type Report struct {
	ReportId   int    `json:"report_id"`
	SessionId  int    `json:"session_id"`
	Detail     string `json:"detail"`
	CreateDate string `json:"created_date"`
}
