package models

type RuleBreaker struct {
	ApplicantId int    `json:"applicant_id"`
	SessionId   int    `json:"session_id"`
	Fault       string `json:"fault"`
	Penalty     string `json:"penalty"`
	CreateDate  string `json:"created_date"`
}
