package models

type Session struct {
	SessionId int    `json:"session_id"`
	RoomId    int    `json:"room_id"`
	Date      string `json:"date"`
	Session_  string `json:"session"`
	Status    string `json:"status"`
}
