package models

type Proctoring struct {
	SessionId int `json:"session_id"`
	ProctorId int `json:"proctor_id"`
}
